// ignore_for_file: prefer_const_constructors

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class NewTaskPage extends StatelessWidget {
  final _txtName = TextEditingController();

  void _onSaved(BuildContext context) {
    FirebaseFirestore
      .instance
      .collection('tasks')
      .add({
        'name': _txtName.text,
        'finished': false,
        'uid': FirebaseAuth.instance.currentUser!.uid,
      });


    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("New Task"),
      ),
      body: Container(
        margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
        child: Column(
          children: [
            TextField(
              controller: _txtName,
              minLines: 1,
              maxLines: 5,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Digite sua tarefa..."
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              width: double.infinity,
              child: ElevatedButton(
                child: Text("Save"),
                onPressed: () => _onSaved(context),
              ),
            ),
          ],
        ),
      ),
    );
  }
}