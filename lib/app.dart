import 'package:flutter/material.dart';

import 'login.page.dart';
import 'newtask.page.dart';
import 'register.page.dart';
import 'tasks.page.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light(
        useMaterial3: false,
        // primarySwatch: Colors.blue,
      ),
      routes: {
        "/login": (context) => LoginPage(),
        "/register": (context) => RegisterPage(),
        "/tasks": (context) => TasksPage(),
        "/newtask": (context) => NewTaskPage(),
      },
      initialRoute: '/login',
    );
  }
}