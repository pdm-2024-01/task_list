import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TasksPage extends StatelessWidget {
  TasksPage({super.key});

  void _logout(BuildContext context) {
    FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, "/login");
  }

  final user = FirebaseAuth.instance.currentUser!;
  final firestore = FirebaseFirestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title: Text("${user.displayName} Tasks"),
        actions: [
          IconButton(
            onPressed: () => _logout(context),
            icon: Icon(Icons.logout),
          ),
        ]
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.pushNamed(context, "/newtask"),
      ),
      body: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
        stream: firestore
          .collection('tasks')
          .where('uid', isEqualTo: user.uid)
          .orderBy('finished')
          .snapshots(),
        builder: (context, snapshot) {

          if(!snapshot.hasData) {
            return const CircularProgressIndicator();
          }

          var docs = snapshot.data!.docs;

          return ListView(
            children: docs.map((doc) => Dismissible(
                background: Container(color: Colors.red),
                onDismissed: (_) => doc.reference.delete(),
                key: Key(doc.id),
                child: CheckboxListTile(
                  title: Text(doc['name']),
                  subtitle: Text("Baixa"),
                  value: doc['finished'],
                  onChanged: (value) => doc.reference.update({'finished': value}),
                ),
              ),).toList(),
          );

        }
      )
    );
  }
}