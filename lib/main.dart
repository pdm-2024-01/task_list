import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

import 'app.dart';

const firebaseConfig  = FirebaseOptions(
  apiKey: "AIzaSyB26YhM8ZRZixdrcXnTy2gnIFUbjwQ6NIQ",
  authDomain: "tasklist-pdm2024-1.firebaseapp.com",
  projectId: "tasklist-pdm2024-1",
  storageBucket: "tasklist-pdm2024-1.appspot.com",
  messagingSenderId: "87436511721",
  appId: "1:87436511721:web:5a5e5ae6d6539258e32378"
);

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firebase.initializeApp(options: firebaseConfig);
  runApp(const App());
}