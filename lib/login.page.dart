// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});

  final txtEmail = TextEditingController();
  final txtPassword = TextEditingController();

  void _signIn(BuildContext context) async {

    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: txtEmail.text,
        password: txtPassword.text,
      );

      Navigator.pushReplacementNamed(context, "/tasks");
    }
    on FirebaseAuthException catch (ex) {
      ScaffoldMessenger
        .of(context)
        .showSnackBar(SnackBar(
          content: Text(ex.message!),
          backgroundColor: Colors.red,
          duration: Duration(seconds: 3),
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 240,
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextField(
                controller: txtEmail,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "E-mail",
                ),
                keyboardType: TextInputType.emailAddress,
              ),
              TextField(
                controller: txtPassword,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Password",
                ),
                obscureText: true,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ElevatedButton(
                  child: Text("SignIn"),
                  onPressed: () => _signIn(context),
                ),
              ),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/register');
                },
                child: Text("New User"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
